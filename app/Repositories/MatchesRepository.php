<?php

namespace App\Repositories;

use Illuminate\Support\Facades\Auth;
use App\Models\Matches;
use App\Models\Team_point;
use Yajra\DataTables\Facades\DataTables;

class MatchesRepository {

    public static function getMatchList($request){
        try {
            $userid = Auth::guard($request->guard)->user()->id;
            $result = Matches::with(['teamA','teamB','result'])->get(); 
            //print_r($result->team_a->name);die;
            return DataTables::of($result)
                    ->addColumn('team_a', function ($data) {
                        return $data->teamA->name;
                    })
                    ->addColumn('team_b', function ($data) {
                        return $data->teamB->name;
                    })
                    ->addColumn('winner', function ($data) {
                        return $data->result->team->name;
                    })
                    ->addColumn('point', function ($data) {
                        return $data->result->point;
                    })
                    ->addColumn('date', function ($data) {
                        return date('d F Y h:i A',strtotime($data->date));
                    })
                    ->escapeColumns(null)
                    ->make(true);
                 
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    /**
     * Add Matches
     */
    public static function addMatch($data)
    {
        try{ 
            $match = new Matches();
            $match->team_a =  $data['team_a'];
            $match->team_b =  $data['team_b'];
            $match->date   =  date('Y-m-d H:i:s');
            
            $match->save();
            return $match;

        } catch (\Exception $ex) {
            throw $ex;
        }  
    }

    /**
     * Add Points for winner team
     */
    public static function addPoint($data)
    {
        try{ 
            $point = new Team_point();
            $point->matche_id =  $data['match_id'];
            $point->team_id =  $data['team_id'];
            $point->point =  $data['point'];
            
            $point->save();
            return $point;

        } catch (\Exception $ex) {
            throw $ex;
        }  
    }
}