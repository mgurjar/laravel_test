<?php

namespace App\Repositories;

use Illuminate\Support\Facades\Auth;
use App\Models\Teams;
use Yajra\DataTables\Facades\DataTables;
use App\Services\StorageService;

class TeamRepository {

    public static function get($request){
        try {
            
            return Teams::where('status','!=','delete')->get();

        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    public static function getTeamList($request){
        try {

            $result = Teams::where('status','!=','delete')->get(); 

            return DataTables::of($result)
                    ->addColumn('logo', function ($data) {
                        if(!empty($data->logo))
                            return '<img src="'.url('/').'/storage/team/'.$data->logo.'" width="60" alt="Team Image">';
                        else
                            return '<img src="'.url('public/assets/images/default-img.png').'" width="60" alt="Team Image">';
                    })
                    ->addColumn('name', function ($data) {
                        return '<a href="'.url('admin/view-team').'/'.$data->id.'">'.$data->name.'</a>';
                    })
                    ->addColumn('club_state', function ($data) {
                        return $data->club_state;
                    })
                    ->addColumn('action', function ($data) {
                        return view('admin.team.team-list-action', ['data' => $data]);
                    })
                    ->escapeColumns(null)
                    ->make(true);
                 
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    public static function getTeamDetail($request){
        try{
            $team = Teams::where(['id'=>$request->id])->where('status','!=','delete')->with('players')->first();
            return $team;
        }catch(\Throwable $th)
        {
            throw $th;
        }
    }

    /**
     * update Team details
     */
    public static function updateTeam($request)
    {
        try{ 
            $team = Teams::find($request->id);
            $team->name=  $request->name;
            if ($request->hasFile('logo')) {
               
                $fileNameToStore = StorageService::uploadTeamImage($request);
                $team->logo =  $fileNameToStore;
            } 
            $team->club_state =  $request->club_state;
            $team->save();
            return $team;
        } catch (\Exception $ex) {
            throw $ex;
        }  
    }


    /**
     * Add Team
     */
    public static function addTeam($request)
    {
        try{ 
            $team = new Teams();
            $team->name=  $request->name;
            if ($request->hasFile('logo')) {
               
                $fileNameToStore = StorageService::uploadTeamImage($request);
                $team->logo =  $fileNameToStore;
            } 
            $team->club_state =  $request->club_state;
            $team->save();
            return $team;
        } catch (\Exception $ex) {
            throw $ex;
        }  
    }

    public static function getTeamA(){
        try {
            return Teams::where(['status'=>'active'])
                    ->where('status','!=','delete')
                    ->inRandomOrder()
                    ->first();

        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    public static function getTeamB($teamA_id){
        try {
            return Teams::where(['status'=>'active'])
                    ->where('status','!=','delete')
                    ->where('id','!=',$teamA_id)
                    ->inRandomOrder()
                    ->first();

        } catch (\Exception $ex) {
            throw $ex;
        }
    }
}