<?php

namespace App\Repositories;

use Illuminate\Support\Facades\Auth;
use App\Models\Players;
use Yajra\DataTables\Facades\DataTables;
use App\Services\StorageService;

class PlayerRepository {

    public static function getPlayerList($request){
        try {
            $userid = Auth::guard($request->guard)->user()->id;
            $result = Players::where('status','!=','delete')->get(); 

            return DataTables::of($result)
                    ->addColumn('image', function ($data) {
                        if(!empty($data->image_url))
                            return '<img src="'.'/storage/player/'.$data->image_url.'" width="60" alt="Player Image">';
                        else
                            return '<img src="'.url('public/assets/images/default-img.png').'" width="60" alt="Player Image">';
                    })
                    ->addColumn('team', function ($data) {
                        return $data->team->name;
                    })
                    ->addColumn('first_name', function ($data) {
                        return $data->first_name;
                    })
                    ->addColumn('last_name', function ($data) {
                        return $data->last_name;
                    })
                    ->addColumn('jersey_number', function ($data) {
                        return $data->jersey_number;
                    })
                    ->addColumn('country', function ($data) {
                        return $data->country;
                    })
                    ->addColumn('status', function ($data) {
                        return $data->status;
                    })
                    ->addColumn('action', function ($data) {
                        return view('admin.player.player-list-action', ['data' => $data]);
                    })
                    ->escapeColumns(null)
                    ->make(true);
                 
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    /**
     * Add Player
     */
    public static function addPlayer($request)
    {
        try{ 
            $player = new Players();
            $player->team_id =  $request->team_id;
            $player->first_name =  $request->first_name;
            $player->last_name =  $request->last_name;
            $player->country =  $request->country;
            $player->jersey_number =  $request->jersey_number;

            if ($request->hasFile('logo')) {
               
                $fileNameToStore = StorageService::uploadPlayerImage($request);
                $player->image_url =  $fileNameToStore;
            }

            $player->save();
            return $player;

        } catch (\Exception $ex) {
            throw $ex;
        }  
    }

    public static function deletePlayer($request){
        try{ 
            
            Players::where(['id'=>$request->id])->delete();
            return true;
            
        } catch (\Exception $ex) {
            throw $ex;
        }  
    }
}