<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Players extends Model {

    protected $table = 'players';
    
    public function team(){
    	return $this->hasOne('App\Models\Teams', 'id', 'team_id');
    }
}
