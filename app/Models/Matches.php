<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Matches extends Model {

    protected $table = 'matches';
    
    public function teamA(){
    	return $this->hasOne('App\Models\Teams', 'id', 'team_a');
    }

    public function teamB(){
    	return $this->hasOne('App\Models\Teams', 'id', 'team_b');
    }

    public function result(){
    	return $this->hasOne('App\Models\Team_point', 'matche_id', 'id');
    }
}
