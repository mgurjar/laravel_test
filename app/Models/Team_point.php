<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Team_point extends Model {

    protected $table = 'team_point';
    
    public function team(){
    	return $this->hasOne('App\Models\Teams', 'id', 'team_id');
    }
}
