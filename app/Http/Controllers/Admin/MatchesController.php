<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\Request;
use Config;
use Lang;

use App\Repositories\MatchesRepository;
use App\Repositories\TeamRepository;

class MatchesController extends Controller
{   

	public function index(Request $request){

		return view('admin.matches.index');
	}

	public function getMatchList(Request $request){
		try{
			return MatchesRepository::getMatchList($request);

		} catch (\Exception $e) {

			return response()->json(
				[
					'success' => false,
					'data' => '',
					'message' => $e->getMessage()
				],
				Config::get('constants.HttpStatus.BAD_REQUEST')
			);
		}
	}


    public function scheduleMatch(){
        try{
            $team_a = TeamRepository::getTeamA();
            if(!empty($team_a)){
                $team_b = TeamRepository::getTeamB($team_a->id);
                if(!empty($team_b)){

                    $data = [
                            'team_a' => $team_a->id,
                            'team_b' => $team_b->id,
                        ];
                    $match = MatchesRepository::addMatch($data);
                    if(!empty($match)){
                        $data = [
                            'team_id' => $team_a->id,
                            'match_id' => $match->id,
                            'point' => 2,
                        ];
                        $point = MatchesRepository::addPoint($data);
                    }

                    return response()->json(
                        [
                            'success' => true,
                            'data' => $point,
                            'message' => Lang::get('success.match.scheduled')
                        ],
                        Config::get('constants.HttpStatus.OK')
                    );

                }
            }

            return response()->json(
                [
                    'success' => false,
                    'data' => "",
                    'message' => Lang::get('success.something_wrong')
                ],
                Config::get('constants.HttpStatus.BAD_REQUEST')
            );

        } catch (\Exception $e) {

            return response()->json(
                [
                    'success' => false,
                    'data' => '',
                    'message' => $e->getMessage()
                ],
                Config::get('constants.HttpStatus.BAD_REQUEST')
            );
        }
    }

}