<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\Request;
use Config;
use Lang;

use App\Repositories\TeamRepository;
use App\Http\Requests\admin\TeamRequest;
class TeamController extends Controller
{   

	public function index(){
		return view('admin.team.index');
	}

	public function getTeamList(Request $request){
		try{
			return TeamRepository::getTeamList($request);

		} catch (\Exception $e) {

			return response()->json(
				[
					'success' => false,
					'data' => '',
					'message' => $e->getMessage()
				],
				Config::get('constants.HttpStatus.BAD_REQUEST')
			);
		}
	}

	/**
     * get detail of team by id
     */
    public function getTeamDetail(Request $request){
        try{
            $result =  TeamRepository::getTeamDetail($request);
          	$data =  view('admin.team.edit-team', ['result' => $result])->render();
            return response()->json(
                [
                    'success' => true,
                    'data' => $data,
                    'message' => ""
                ],
                Config::get('constants.HttpStatus.OK')
            );
        }catch(\Exception $e){
            return response()->json(
                [
                    'success' => false,
                    'data' => '',
                    'message' => $e->getMessage()
                ],
                Config::get('constants.HttpStatus.BAD_REQUEST')
            );
        }
    }

    /**
    * update Team details 
    */
	public function updateTeam(TeamRequest $request)
	{
       try{  
           $result =  TeamRepository::updateTeam($request);

           if(!empty($result)){
               return response()->json(
                   [
                       'success' => true,
                       'data' => $result,
                       'message' => Lang::get('success.team.updated')
                   ],
                   Config::get('constants.HttpStatus.OK')
               );
           }else{
               return response()->json(
                   [
                       'success' => false,
                       'data' => "",
                       'message' => Lang::get('success.something_wrong')
                   ],
                   Config::get('constants.HttpStatus.BAD_REQUEST')
               );
           }
           } catch (\Exception $e) {
               return response()->json(
                   [
                       'success' => false,
                       'data' => '',
                       'message' => $e->getMessage()
                   ],
                   Config::get('constants.HttpStatus.BAD_REQUEST')
               );
           }
   	}

    /**
    * update Team details 
    */
    public function addTeam(TeamRequest $request)
    {
       try{  
           $result =  TeamRepository::addTeam($request);

           if(!empty($result)){
               return response()->json(
                   [
                       'success' => true,
                       'data' => $result,
                       'message' => Lang::get('success.team.created')
                   ],
                   Config::get('constants.HttpStatus.OK')
               );
           }else{
               return response()->json(
                   [
                       'success' => false,
                       'data' => "",
                       'message' => Lang::get('success.something_wrong')
                   ],
                   Config::get('constants.HttpStatus.BAD_REQUEST')
               );
           }
           } catch (\Exception $e) {
               return response()->json(
                   [
                       'success' => false,
                       'data' => '',
                       'message' => $e->getMessage()
                   ],
                   Config::get('constants.HttpStatus.BAD_REQUEST')
               );
           }
    }

    /*
    * View team details with player
    */
    public function viewTeam(Request $request){
        try{
            $result =  TeamRepository::getTeamDetail($request);
            
            return view('admin.team.view-team', ['team' => $result]);
            
        }catch(\Exception $e){
            return response()->json(
                [
                    'success' => false,
                    'data' => '',
                    'message' => $e->getMessage()
                ],
                Config::get('constants.HttpStatus.BAD_REQUEST')
            );
        }
    }
}