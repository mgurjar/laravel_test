<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\Request;
use Config;
use Lang;

use App\Repositories\PlayerRepository;
use App\Repositories\TeamRepository;
use App\Http\Requests\admin\PlayerRequest;
class PlayerController extends Controller
{   

	public function index(Request $request){
        $teams = TeamRepository::get($request);
		return view('admin.player.index',['teams'=>$teams]);
	}

	public function getPlayerList(Request $request){
		try{
			return PlayerRepository::getPlayerList($request);

		} catch (\Exception $e) {

			return response()->json(
				[
					'success' => false,
					'data' => '',
					'message' => $e->getMessage()
				],
				Config::get('constants.HttpStatus.BAD_REQUEST')
			);
		}
	}


    /**
    * update Team details 
    */
    public function addPlayer(PlayerRequest $request)
    {
       try{  
           $result =  PlayerRepository::addPlayer($request);

           if(!empty($result)){
               return response()->json(
                   [
                       'success' => true,
                       'data' => $result,
                       'message' => Lang::get('success.player.created')
                   ],
                   Config::get('constants.HttpStatus.OK')
               );
           }else{
               return response()->json(
                   [
                       'success' => false,
                       'data' => "",
                       'message' => Lang::get('success.something_wrong')
                   ],
                   Config::get('constants.HttpStatus.BAD_REQUEST')
               );
           }
        } catch (\Exception $e) {
           return response()->json(
               [
                   'success' => false,
                   'data' => '',
                   'message' => $e->getMessage()
               ],
               Config::get('constants.HttpStatus.BAD_REQUEST')
           );
       }
    }

    public function deletePlayer(Request $request){
        try{  
           $result =  PlayerRepository::deletePlayer($request);

           if(!empty($result)){
               return response()->json(
                   [
                       'success' => true,
                       'data' => $result,
                       'message' => Lang::get('success.player.deleted')
                   ],
                   Config::get('constants.HttpStatus.OK')
               );
           }else{
               return response()->json(
                   [
                       'success' => false,
                       'data' => "",
                       'message' => Lang::get('success.something_wrong')
                   ],
                   Config::get('constants.HttpStatus.BAD_REQUEST')
               );
           }
        } catch (\Exception $e) {
           return response()->json(
               [
                   'success' => false,
                   'data' => '',
                   'message' => $e->getMessage()
               ],
               Config::get('constants.HttpStatus.BAD_REQUEST')
           );
        }
    }

}