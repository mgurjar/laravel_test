<?php

namespace App\Http\Requests\admin;

use Illuminate\Foundation\Http\FormRequest;

class PlayerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'team_id' => 'required',
            'first_name' => 'required',
            'last_name' => 'required',
            'country' => 'required',
            'jersey_number' => 'required',
            'logo' => 'nullable|sometimes|mimes:jpeg,png,jpg|required|max:5000',
            
        ];
    }
    /**
     * Custom validation messages
     */
    public function messages()
    {
        return [
            'team_id.required' => 'Team field is required.',
            'first_name.required' => 'First name field is required.',
            'last_name.required' => 'Last name field is required.',
            'country.required' => 'Country field is required.',
            'jersey_number.required' => 'Jersey number field is required.',
            'logo.mimes'=> 'Please add either a JPEG, JPG or a PNG file not exceeding 5Mbs',
            'logo.image'=> 'Please add either a JPEG, JPG or a PNG file not exceeding 5Mbs',
            'logo.max'=> 'Please add either a JPEG, JPG or a PNG file not exceeding 5Mbs',
        ];
    }
 
}
