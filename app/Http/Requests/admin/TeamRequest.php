<?php

namespace App\Http\Requests\admin;

use Illuminate\Foundation\Http\FormRequest;

class TeamRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'club_state' => 'required',
            'logo' => 'nullable|sometimes|mimes:jpeg,png,jpg|required|max:5000',
            
        ];
    }
    /**
     * Custom validation messages
     */
    public function messages()
    {
        return [
            'name.required' => 'Name field is required.',
            'club_state.required' => 'Club state field is required.',
            'logo.mimes'=> 'Please add either a JPEG, JPG or a PNG file not exceeding 5Mbs',
            'logo.image'=> 'Please add either a JPEG, JPG or a PNG file not exceeding 5Mbs',
            'logo.max'=> 'Please add either a JPEG, JPG or a PNG file not exceeding 5Mbs',
        ];
    }
 
}
