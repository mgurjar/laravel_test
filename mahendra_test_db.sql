-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jul 07, 2020 at 02:23 PM
-- Server version: 10.4.10-MariaDB
-- PHP Version: 7.2.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mahendra_test_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `matches`
--

DROP TABLE IF EXISTS `matches`;
CREATE TABLE IF NOT EXISTS `matches` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `team_a` int(11) NOT NULL,
  `team_b` int(11) NOT NULL,
  `date` datetime DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `matches`
--

INSERT INTO `matches` (`id`, `team_a`, `team_b`, `date`, `created_at`, `updated_at`) VALUES
(1, 3, 2, '2020-06-20 17:20:49', '2020-06-20 11:50:49', '2020-06-20 11:50:49'),
(2, 3, 1, '2020-06-20 17:20:59', '2020-06-20 11:50:59', '2020-06-20 11:50:59'),
(3, 2, 1, '2020-07-07 14:11:52', '2020-07-07 08:41:52', '2020-07-07 08:41:52');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `players`
--

DROP TABLE IF EXISTS `players`;
CREATE TABLE IF NOT EXISTS `players` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `team_id` int(11) NOT NULL,
  `first_name` varchar(256) DEFAULT NULL,
  `last_name` varchar(256) DEFAULT NULL,
  `image_url` varchar(256) DEFAULT NULL,
  `jersey_number` varchar(10) DEFAULT NULL,
  `country` varchar(50) DEFAULT NULL,
  `status` enum('active','inactive','delete') NOT NULL DEFAULT 'active',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `team_id` (`team_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `players`
--

INSERT INTO `players` (`id`, `team_id`, `first_name`, `last_name`, `image_url`, `jersey_number`, `country`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 'Ravindra', 'Jadeja', 'images--1-_1594131309.jpg', '123', 'IND', 'active', '2020-07-07 14:21:08', '2020-06-20 11:46:16'),
(2, 1, 'Abhishek', 'Nayar', 'images--1-_1594131309.jpg', '22', 'IND', 'active', '2020-07-07 14:21:10', '2020-06-20 11:46:43'),
(3, 2, 'Sudeep', 'Tyagi', 'images--1-_1594131309.jpg', '55', 'IND', 'active', '2020-07-07 14:21:13', '2020-06-20 11:47:12'),
(4, 2, 'Ishant', 'Sharma', 'images--1-_1594131309.jpg', '77', 'IND', 'active', '2020-07-07 14:21:16', '2020-06-20 11:49:39'),
(5, 3, 'Murali', 'Vijay', 'images--1-_1594131309.jpg', '99', 'IND', 'active', '2020-07-07 14:21:18', '2020-06-20 11:50:05'),
(6, 3, 'Shikhar', 'Dhawan', 'images--1-_1594131309.jpg', '89', 'IND', 'active', '2020-07-07 14:21:20', '2020-06-20 11:50:35'),
(7, 4, 'Test Player', 'Player', 'images--1-_1594131309.jpg', '1003', 'IND', 'active', '2020-07-07 08:45:09', '2020-07-07 08:45:09');

-- --------------------------------------------------------

--
-- Table structure for table `teams`
--

DROP TABLE IF EXISTS `teams`;
CREATE TABLE IF NOT EXISTS `teams` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) NOT NULL,
  `logo` varchar(250) DEFAULT NULL,
  `club_state` varchar(256) DEFAULT NULL,
  `status` enum('active','inactive','delete') NOT NULL DEFAULT 'active',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `teams`
--

INSERT INTO `teams` (`id`, `name`, `logo`, `club_state`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Chennai Super Kings', 'images_1594131246.jpg', 'Chennai', 'active', '2020-06-20 11:27:39', '2020-06-20 11:27:39'),
(2, 'Delhi Capitals', 'images_1594131246.jpg', 'Delhi', 'active', '2020-06-20 11:27:54', '2020-06-20 11:27:54'),
(3, 'Kings XI Punjab', 'images_1594131246.jpg', 'Punjab', 'active', '2020-06-20 11:31:15', '2020-06-20 11:31:15'),
(4, 'Test Team', 'images_1594131246.jpg', 'MP', 'active', '2020-07-07 08:44:07', '2020-07-07 08:52:44');

-- --------------------------------------------------------

--
-- Table structure for table `team_point`
--

DROP TABLE IF EXISTS `team_point`;
CREATE TABLE IF NOT EXISTS `team_point` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `matche_id` int(11) NOT NULL,
  `team_id` int(11) NOT NULL,
  `point` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `matche_id` (`matche_id`),
  KEY `team_id` (`team_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `team_point`
--

INSERT INTO `team_point` (`id`, `matche_id`, `team_id`, `point`, `created_at`, `updated_at`) VALUES
(1, 1, 3, 2, '2020-06-20 11:50:49', '2020-06-20 11:50:49'),
(2, 2, 3, 2, '2020-06-20 11:50:59', '2020-06-20 11:50:59'),
(3, 3, 2, 2, '2020-07-07 08:41:52', '2020-07-07 08:41:52');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('active','inactive','delete') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `status`, `created_at`, `updated_at`) VALUES
(2, 'admin', 'admin@gmail.com', '$2y$10$6myMJdsVw0tBh.z9B/EfV.lZo4D82WwCQSQmCMqEp1Aen83DaLMoy', NULL, 'active', '2020-06-19 14:18:24', '2020-06-19 14:18:24');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `players`
--
ALTER TABLE `players`
  ADD CONSTRAINT `players_ibfk_1` FOREIGN KEY (`team_id`) REFERENCES `teams` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `team_point`
--
ALTER TABLE `team_point`
  ADD CONSTRAINT `team_point_ibfk_1` FOREIGN KEY (`matche_id`) REFERENCES `matches` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `team_point_ibfk_2` FOREIGN KEY (`team_id`) REFERENCES `teams` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
