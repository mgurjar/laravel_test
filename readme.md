
## Requirements

- PHP >= 7.0


## Database Setup

Setup database 

- Find database file in root directory "mahendra_test_db".
- Import database.
- Change database setting in .env file.



## Installation

- Run command "composer install".
- Run command "composer dump-autoload".

## Link Storage path
- Run command "php artisan storage:link".

## Run Project
- Run command "php artisan serve".

## Login details

- Admin login
- email : admin@gmail.com
- password : test123123
