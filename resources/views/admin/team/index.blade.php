@extends('admin.layouts.app')
    @section('head')
    <title>Manage Team</title>
    @endsection
@section('content')
    <main class="admin-main manageSubadmin">
    	<div class="admin_pageContent">
            
            <!-- page title -->
            <div class="adminPageTitle d-sm-flex align-items-sm-center justify-content-sm-between">
                <div class="adminPageTitle__left">
                    <h1>Manage Team</h1>
                    <nav aria-label="breadcrumb">
                      <ol class="breadcrumb">
                        <li class="breadcrumb-item active" aria-current="page">Manage Team</li>
                      </ol>
                    </nav>
                </div>
                <div class="adminPageTitle__right">
                    <a href="javascript:void(0);" class="btn btn-danger btn-sm ripple-effect addTeam">Add Team</a>
                </div>
            </div>
            <div class="table-responsive commonTable bg-white">
                <table class="table" id="teamList">
                    <thead>
                        <tr>
                            <th>S.no</th>
                            <th>Logo</th>
                            <th>Team Name</th>
                            <th>Club State</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                </table>
            </div>
    	</div>
    </main>

    <!-- edit team -->
    <div class="modal fade addLanguage adminModal" data-backdrop="static" id="editTeam" tabindex="-1" role="dialog" aria-labelledby="addTeamLabel" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="addTeamLabel">Edit Team</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true"><i class="icon-close"></i></span>
            </button>
          </div>
          <div class="modal-body">
            <form id="edit-team-form">
             

            </form>
          </div>
        </div>
      </div>
    </div>

    <!-- Add team -->
    <div class="modal fade addLanguage adminModal" data-backdrop="static" id="addTeam" tabindex="-1" role="dialog" aria-labelledby="addTeamLabel" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="addTeamLabel">Add Team</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true"><i class="icon-close"></i></span>
            </button>
          </div>
          <div class="modal-body">
            <form id="add-team-form">
                <div class="input-group form-group">
                    <div class="custom-file h-auto">
                        <input type="file" class="custom-file-input form-control" id="inputGroupFile02"  name="logo" aria-describedby="inputGroupFileAddon02">
                        <label class="custom-file-label form-control" for="inputGroupFile02">Choose Image</label>                        
                    </div>
                    <div class="error-help-block text-danger"></div>
                </div>

                <div class="form-group focusField">
                    <input type="text" class="form-control" placeholder="Name" name="name" value="">
                    <label>Name</label>
                    <div class="error-help-block text-danger"></div>
                </div>
                 
                <div class="form-group focusField">
                    <input type="text" class="form-control" placeholder="Club State" name="club_state" value="">
                    <label>Club State</label>
                    <div class="error-help-block text-danger"></div>
                </div>

                <button type="submit" id="submitBtn" class="btn btn-danger btn-lg ripple-effect w-100 btn-add-team">Add<span class="spinner-border" role="status" style="display:none"></span></button>
            </form>
          </div>
        </div>
      </div>
    </div>

@endsection
@section('js')
<script>
    function listTeam() {
        $('#teamList').DataTable({ 
            pageLength: 10,
            order: [
                [0, "desc"]
            ],
            processing: true,
            bFilter: false,
            lengthChange: false,
            bInfo: false,
            language: {
                processing: '<div class="listloader text-center"><span class="spinner-border" role="status"></span></div>',
                emptyTable: 'No record found.'
            },
            serverSide: true,
            ajax: {
                url: "{{ route('admin/getTeamList') }}",
                type: "GET",
                data: function(d) {
                    d.name = $('#searchKey').val();
                    currentPage = d.start 
                }
            },
            columns: [
                {
                    data: 'id',
                    render: function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {
                    data: 'logo',
                    name: 'logo'
                },
                {
                    data: 'name',
                    name: 'name'
                },
                {
                    data: 'club_state',
                    name: 'club_state'
                },
                {
                    data: 'action',
                    name: 'action'
                }
            ]
        });
    }
    listTeam();

    // Start Add team
    $('.addTeam').click(function(event) {
        $('#addTeam').modal('show');
    });
    $(document).on('click','.btn-add-team',function(e){
        e.preventDefault();
        var button = $(this);
        button.attr('disabled', true);
        button.find('span').show();

        var form = $("#add-team-form").get(0);
        var datas = new FormData(form);
        $.ajax({
            type: "POST",
            url: "{{ route('admin/addTeam') }}",
            data: datas,
            processData: false,
            contentType: false,
            success: function(data) {
                button.attr('disabled', false);
                button.find('span').hide();
                if (data.success) {
                    _toast.success(data.message);
                    $('#add-team-form').trigger('reset');
                    $('#addTeam').modal('hide');
                    $('#teamList').DataTable().ajax.reload(null, false);                       
                }
            },
            error: function(data) {
                button.attr('disabled', false);
                button.find('span').hide();
                if (data.status === 422) {
                    var obj = data.responseJSON;
                    for (var x in obj.errors) {
                        $('#add-team-form [name=' + x + ']')
                            .closest('.form-group')
                            .find('.error-help-block')
                            .html(obj.errors[x].join('<br>'));
                      
                            if(x=="menus"){
                                $(".error-help-block-"+x).html(obj.errors[x].join('<br>'));
                            }
                    }
                } else if (data.status === 400) {
                    _toast.error(data.message);
                }
            }
        });
    });

    // Edit team 
    $(document).on('click',".edit-team-modal",function(){
        var id=$(this).data('key');
        $.ajax({
            type: "POST",
            url: "{{ route('admin/getTeamDetail') }}",
            data: {id : id},
            success: function(data) {
                if (data.success) {
                    $('#editTeam').modal('show');
                    $("#edit-team-form").html(data.data);
                    
                }
            },
            error: function(data) {                    
              
            }
        });
    })

    $(document).on('click','.btn-edit-team',function(e){
        e.preventDefault();
        var button = $(this);
        button.attr('disabled', true);
        button.find('span').show();

        var form = $("#edit-team-form").get(0);
        var datas = new FormData(form);
        $.ajax({
            type: "POST",
            url: "{{ route('admin/updateTeam') }}",
            data: datas,
            processData: false,
            contentType: false,
            success: function(data) {
                button.attr('disabled', false);
                button.find('span').hide();
                if (data.success) {
                    _toast.success(data.message);
                    $('#edit-team-form').trigger('reset');
                    $('#editTeam').modal('hide');
                    $('#teamList').DataTable().ajax.reload(null, false);                       
                }
            },
            error: function(data) {
                button.attr('disabled', false);
                button.find('span').hide();
                if (data.status === 422) {
                    var obj = data.responseJSON;
                    for (var x in obj.errors) {
                        $('#edit-team-form [name=' + x + ']')
                            .closest('.form-group')
                            .find('.error-help-block')
                            .html(obj.errors[x].join('<br>'));
                      
                            if(x=="menus"){
                                $(".error-help-block-"+x).html(obj.errors[x].join('<br>'));
                            }
                    }
                } else if (data.status === 400) {
                    _toast.error(data.message);
                }
            }
        });
    });
</script>
@endsection