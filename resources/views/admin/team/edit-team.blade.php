<div class="input-group form-group">
    <div class="custom-file h-auto">
        <input type="file" class="custom-file-input form-control" id="inputGroupFile02"  name="logo" aria-describedby="inputGroupFileAddon02">
        <label class="custom-file-label form-control" for="inputGroupFile02">Choose Image</label>                        
    </div>
    <div class="error-help-block text-danger"></div>
</div>

<input type="hidden" name="id" value="{{$result->id}}">

<div class="form-group focusField">
    <input type="text" class="form-control" placeholder="Name" name="name" value="{{$result->name}}">
    <label>Name</label>
    <div class="error-help-block text-danger"></div>
</div>
 
<div class="form-group focusField">
    <input type="text" class="form-control" placeholder="Club State" name="club_state" value="{{$result->club_state}}">
    <label>Club State</label>
    <div class="error-help-block text-danger"></div>
</div>

<button type="submit" id="submitBtn" class="btn btn-danger btn-lg ripple-effect w-100 btn-edit-team">Update<span class="spinner-border" role="status" style="display:none"></span></button>