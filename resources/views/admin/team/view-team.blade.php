@extends('admin.layouts.app') 
@section('head')
<title>View Team</title>
@endsection
@section('content')
<main class="admin-main patientDetail">
    <div class="admin_pageContent">
        
        <!-- page title -->
        <div class="adminPageTitle d-sm-flex align-items-sm-center justify-content-sm-between">
            <div class="adminPageTitle__left">
                <h1>Team Detail</h1>
                <nav aria-label="breadcrumb">
                  <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('admin/team') }}">Manage Team</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Team Detail</li>
                  </ol>
                </nav>
            </div>
        </div>

        <div class="personal  bg-white p-30 boxShadow ">
            <h5 class="font-bd commonHeading">Team Details</h5>
            <div class="d-sm-flex align-items-start">
                <div class="profile">
                    <div class="profile__img">
                        <img src="{{ url('public/assets/images/default-img.png') }}" class="rounded-circle" alt="patient Profile">
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-12">
                        <div class="commonList mb-2 mb-md-0">
                            <ul class="list-unstyled fullwidth d-flex flex-wrap mb-0">
                                <li>
                                    <label>Team Name:</label>
                                    <span>{{ $team->name }}</span>
                                </li>
                                <li class="list-inline-item">
                                    <label>Club State</label>
                                    <span>{{ $team->club_state }}</span>
                                </li>
                                
                                
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="commonTabs">
            <ul class="nav nav-pills bg-white boxShadow" id="pills-tab" role="tablist">
                <li class="nav-item" role="presentation">
                   <a class="nav-link active" id="pills-player-tab" data-toggle="pill" href="#pills-player" role="tab" aria-controls="pills-player" aria-selected="false">Players</a>
                </li>
            </ul>
            <div class="tab-content" id="pills-tabContent">

                <div class="tab-pane fade show active" id="pills-player" role="tabpanel" aria-labelledby="pills-player-tab">
                   
                    <div class="table-responsive commonTable">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Player Name</th>
                                    <th>Amount </th>
                                    <th>Sent on (Date)</th>
                                    <th>Paid on (Date)</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody id="payment">
                                <tr>
                                    <td>
                                        <div class="user d-flex align-items-center">
                                            <div class="user__img rounded-circle">
                                                <img src="{{ url('public/assets/images/default-img.png') }}" alt="doctor" >
                                            </div>
                                            <div class="user__caption">
                                                <h6>James Zathila</h6>
                                            </div>
                                        </div>
                                    </td>
                                    <td>$1000</td>
                                    <td>06/14/2020</td>
                                    <td>06/20/2020</td>
                                    <td class="text-success">
                                        Paid
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="user d-flex align-items-center">
                                            <div class="user__img rounded-circle">
                                                <img src="{{ url('public/assets/images/default-img.png') }}" alt="doctor" >
                                            </div>
                                            <div class="user__caption">
                                                <h6>Linda Barrett</h6>
                                            </div>
                                        </div>
                                    </td>
                                    <td>$500</td>
                                    <td>05/20/2020</td>
                                    <td>05/25/2020</td>
                                    <td class="text-success">
                                        Paid
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="user d-flex align-items-center">
                                            <div class="user__img rounded-circle">
                                                <img src="{{ url('public/assets/images/default-img.png') }}" alt="doctor" >
                                            </div>
                                            <div class="user__caption">
                                                <h6>Cristina Groves</h6>
                                            </div>
                                        </div>
                                    </td>
                                    <td>$300</td>
                                    <td>-</td>
                                    <td>-</td>
                                    <td class="text-danger">
                                        Not Paid
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="user d-flex align-items-center">
                                            <div class="user__img rounded-circle">
                                                <img src="{{ url('public/assets/images/default-img.png') }}" alt="doctor" >
                                            </div>
                                            <div class="user__caption">
                                                <h6>Michael Sullivan</h6>
                                            </div>
                                        </div>
                                    </td>
                                    <td>$500</td>
                                    <td>-</td>
                                    <td>-</td>
                                    <td class="text-danger">
                                        Not Paid
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="d-flex align-items-center paginationBottom justify-content-end">
                        <nav aria-label="Page navigation example ">
                          <ul class="pagination justify-content-end ">
                            <li class="page-item disabled">
                              <a class="page-link" href="#" tabindex="-1" aria-disabled="true"><i class="icon-chevron-left"></i></a>
                            </li>
                            <li class="page-item"><a class="page-link" href="#">1</a></li>
                            <li class="page-item"><span>...</span></li>
                            <li class="page-item"><a class="page-link" href="#">4</a></li>
                            <li class="page-item"><a class="page-link" href="#">5</a></li>
                            <li class="page-item">
                              <a class="page-link" href="#"><i class="icon-chevron-right"></i></a>
                            </li>
                          </ul>
                        </nav>
                    </div>
                </div>

            </div>
        </div>


    </div>

</main>
@endsection 