<aside class="sideMenu">
	<ul class="list-unstyled mb-0">
		
		<li>
			<a href="{{ url('admin/teams')  }}" class="sideMenu__link {{ ( \Request::route()->getName() === 'admin/team' || \Request::route()->getName() === 'admin/viewTeam') ? 'active' : ''  ? 'active' : ''  }}"> 
				<i class="icon-subadmin"></i> Manage Teams
			</a>
		</li>

		<li>
			<a href="{{ url('admin/players')  }}" class="sideMenu__link {{ ( \Request::route()->getName() === 'admin/player' ) ? 'active' : ''  ? 'active' : ''  }}"> 
				<i class="icon-subadmin"></i> Manage Player
			</a>
		</li>
		
		<li>
			<a href="{{ url('admin/matches')  }}" class="sideMenu__link {{ ( \Request::route()->getName() === 'admin/matches' ) ? 'active' : ''  ? 'active' : ''  }}"> 
				<i class="icon-subadmin"></i> Manage Matches
			</a>
		</li>

	</ul>
</aside>
