@extends('admin.layouts.app')
    @section('head')
    <title>Manage Matches</title>
    @endsection
@section('content')
    <main class="admin-main manageSubadmin">
    	<div class="admin_pageContent">
            
            <!-- page title -->
            <div class="adminPageTitle d-sm-flex align-items-sm-center justify-content-sm-between">
                <div class="adminPageTitle__left">
                    <h1>Manage Matches</h1>
                    <nav aria-label="breadcrumb">
                      <ol class="breadcrumb">
                        <li class="breadcrumb-item active" aria-current="page">Manage Matches</li>
                      </ol>
                    </nav>
                </div>
                <div class="adminPageTitle__right">
                    <a href="javascript:void(0);" class="btn btn-danger btn-sm ripple-effect scheduleMatch">Schedule Match</a>
                </div>
            </div>
            <div class="table-responsive commonTable bg-white">
                <table class="table" id="matcheList">
                    <thead>
                        <tr>
                            <th>S.no</th>
                            <th>Team A</th>
                            <th>Team B</th>
                            <th>Winner Team</th>
                            <th>Points</th>
                            <th>Mtch Date</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                </table>
            </div>
    	</div>
    </main>


@endsection
@section('js')
<script>
    function listMatches() {
        $('#matcheList').DataTable({ 
            pageLength: 10,
            order: [
                [0, "desc"]
            ],
            processing: true,
            bFilter: false,
            lengthChange: false,
            bInfo: false,
            language: {
                processing: '<div class="listloader text-center"><span class="spinner-border" role="status"></span></div>',
                emptyTable: 'No record found.'
            },
            serverSide: true,
            ajax: {
                url: "{{ route('admin/getMatchList') }}",
                type: "GET",
                data: function(d) {
                    d.name = $('#searchKey').val();
                    currentPage = d.start 
                }
            },
            columns: [
                {
                    data: 'id',
                    render: function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {
                    data: 'team_a',
                    name: 'team_a'
                },
                {
                    data: 'team_b',
                    name: 'team_b'
                },
                {
                    data: 'winner',
                    name: 'winner'
                },
                {
                    data: 'point',
                    name: 'point'
                },
                {
                    data: 'date',
                    name: 'date'
                }
            ]
        });
    }
    listMatches();

    // Start Add Matches
    
    $(document).on('click','.scheduleMatch',function(e){
        e.preventDefault();
        var button = $(this);
        button.attr('disabled', true);
        button.find('span').show();
        $.ajax({
            type: "GET",
            url: "{{ route('admin/scheduleMatch') }}",
            //data: {},
            processData: false,
            contentType: false,
            success: function(data) {
                button.attr('disabled', false);
                button.find('span').hide();
                if (data.success) {
                    _toast.success(data.message);
                    $('#matcheList').DataTable().ajax.reload(null, false);                    
                }else{
                    _toast.error(data.message);
                }
            },
            error: function(data) {

                button.attr('disabled', false);
                button.find('span').hide();
                
                _toast.error(data.message);
                
            }
        });
    });

</script>
@endsection