<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::middleware(['guest.admin'])->group(function () {
    Route::get('/', 'admin\LoginController@showLoginPage');
    Route::get('/login', 'admin\LoginController@showLoginPage')->name('admin/showLoginPage');
    Route::post('/login', 'admin\LoginController@login')->name('admin/login');
 
});

Route::middleware(['auth.admin'])->group(function () {
    
    /*
    * Manage Team Routes
    */
    Route::get('/teams', 'admin\TeamController@index')->name('admin/team');
    Route::get('get-team-list', 'admin\TeamController@getTeamList')->name('admin/getTeamList');
    Route::post('/get-team-detail', 'admin\TeamController@getTeamDetail')->name('admin/getTeamDetail');
    Route::post('/update-team', 'admin\TeamController@updateTeam')->name('admin/updateTeam');
    Route::post('/add-team', 'admin\TeamController@addTeam')->name('admin/addTeam');
    Route::get('/view-team/{id}', 'admin\TeamController@viewTeam')->name('admin/viewTeam');

    /*
    * Manage player routes
    */
    Route::get('/players', 'admin\PlayerController@index')->name('admin/player');
    Route::get('/get-player-list', 'admin\PlayerController@getPlayerList')->name('admin/getPlayerList');
    Route::post('/add-player-list', 'admin\PlayerController@addPlayer')->name('admin/addPlayer');
    Route::post('/delete-player', 'admin\PlayerController@deletePlayer')->name('admin/deletePlayer');

    /*
    * Manage matches routes
    */
    Route::get('/matches', 'admin\MatchesController@index')->name('admin/matches');
    Route::get('/get-match-list', 'admin\MatchesController@getMatchList')->name('admin/getMatchList');
    Route::get('/schedule-match', 'admin\MatchesController@scheduleMatch')->name('admin/scheduleMatch');

});

Route::get('/logout', 'admin\LoginController@logout')->name('admin/logout');